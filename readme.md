## Installation
create .env file from .env.example
create /client/.env file from /client/.env.example

##add SHU project
for add run this command
```
$ npm run copy-shu path={SHU directory name}
```

## Global packages
install "recursive-copy-cli" package as global for copy SHU
```
$ npm install -g recursive-copy-cli
```
##Front end

cd client
yarn install

for view front part run
```bash
$ yarn run build
```

## Running the app
```bash
#compiling SHU run this command
$ npm run build
```

#current project

# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

##API Documentation

{host}/swagger-ui
