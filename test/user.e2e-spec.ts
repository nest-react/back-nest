import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { UserModule } from '../src/resources/user/user.module';
import { UserService } from '../src/resources/user/user.service';
import { INestApplication } from '@nestjs/common';
import { ConfigModule, ConfigService } from "@nestjs/config";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AuthModule } from "../src/auth/auth.module";
import axios from "axios";
import { SnakeNamingStrategy } from "typeorm-naming-strategies";
import { CONNECTION_LOCAL } from "../src/constants/Connection.constants";

describe('User', () => {
    let token = '';
    let app: INestApplication;
    let item = {
        id: 1,
        name: "xx",
        username: "www",
        administrator: true,
        active: false,
        created_at: "2020-11-30T09:22:47.668Z"
    };
    let userService = {
        findAll: () => ({
            items: [item],
            meta: {
                totalItems: 1,
                itemCount: 1,
                itemsPerPage: 10,
                totalPages: 1,
                currentPage: 1
            }
        }),
        findOne: () => item
    };

    beforeAll(async () => {
        const moduleRef = await Test.createTestingModule({
            imports: [
                ConfigModule.forRoot({
                    envFilePath: '.env',
                    isGlobal: true
                }),
                UserModule,
                AuthModule,
                ConfigModule.forRoot({
                    envFilePath: '.env',
                    isGlobal: true
                }),
                TypeOrmModule.forRootAsync({
                    imports: [
                        ConfigModule
                    ],
                    inject: [ConfigService],
                    name: 'local',
                    useFactory: (configService: ConfigService) => {
                        return {
                            name: CONNECTION_LOCAL,
                            type: 'postgres',
                            host: configService.get<string>("DB_HOST", "localhost"),
                            port: configService.get<number>("DB_PORT", 5432),
                            username: configService.get<string>("DB_USERNAME", "postgres"),
                            password: configService.get<string>("DB_PASSWORD", "postgres"),
                            database: configService.get<string>("DB_DATABASE_NAME", "shu_api"),
                            synchronize: true,
                            namingStrategy: new SnakeNamingStrategy(),
                            logging: configService.get<string>("NODE_ENV", "development") === "development",
                            entities: [process.cwd() + '/src/models/**.entity.ts']
                        }
                    }
                }),
            ],
        })
            .overrideProvider(UserService)
            .useValue(userService)
            .compile();

        app = moduleRef.createNestApplication();
        let c = await app.init();

        try {
            let data = await axios.post('http://localhost:8080/api/login', {
                apis: ['all'],
                username: 'test_user',
                password: '123!@#qweQWE'
            });
            token = data.data.access_token;
        } catch (e) {
            console.log(e)
        }
    });

    it(`/GET users`, () => {
        return request(app.getHttpServer())
            .get('/user')
            .auth(token, { type: 'bearer'})
            .expect(200)
            .expect(userService.findAll());
    });

    it(`/GET user/:id`, () => {
        return request(app.getHttpServer())
            .get('/user/1')
            .auth(token, { type: 'bearer'})
            .expect(200)
            .expect(userService.findOne());
    });

    afterAll(async () => {
        await app.close();
    });
});