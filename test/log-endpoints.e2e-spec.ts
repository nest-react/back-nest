import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { LogEndpointsService } from '../src/resources/log-endpoints/log-endpoints.service';
import { INestApplication } from '@nestjs/common';
import { ConfigModule, ConfigService } from "@nestjs/config";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AuthModule } from "../src/auth/auth.module";
import { LogEndpointsModule } from "../src/resources/log-endpoints/log-endpoints.module";
import axios from "axios";
import { SnakeNamingStrategy } from "typeorm-naming-strategies";
import { CONNECTION_LOCAL } from "../src/constants/Connection.constants";

describe('Log Endpoints', () => {
    let token = '';
    let app: INestApplication;
    let item = {
        id: 69,
        type: "login",
        info: {
            route: "/login",
            method: "POST"
        },
        time: 15,
        created_at: "2020-12-02T09:48:49.082Z",
        user: 1
    };
    let meta = {
        totalItems: 1,
        itemCount: 1,
        itemsPerPage: 10,
        totalPages: 1,
        currentPage: 1
    };
    let logEndpointService = {
        findAll: () => ({
            items: [item],
            meta
        }),
        findAllByUserId: () => ({
            items: [item],
            meta
        }),
        findOne: () => item
    };

    beforeAll(async () => {
        const moduleRef = await Test.createTestingModule({
            imports: [
                ConfigModule.forRoot({
                    envFilePath: '.env',
                    isGlobal: true
                }),
                AuthModule,
                LogEndpointsModule,
                ConfigModule.forRoot({
                    envFilePath: '.env',
                    isGlobal: true
                }),
                TypeOrmModule.forRootAsync({
                    imports: [
                        ConfigModule
                    ],
                    inject: [ConfigService],
                    name: 'local',
                    useFactory: (configService: ConfigService) => {
                        return {
                            name: CONNECTION_LOCAL,
                            type: 'postgres',
                            host: configService.get<string>("DB_HOST", "localhost"),
                            port: configService.get<number>("DB_PORT", 5432),
                            username: configService.get<string>("DB_USERNAME", "postgres"),
                            password: configService.get<string>("DB_PASSWORD", "postgres"),
                            database: configService.get<string>("DB_DATABASE_NAME", "shu_api"),
                            synchronize: true,
                            namingStrategy: new SnakeNamingStrategy(),
                            logging: configService.get<string>("NODE_ENV", "development") === "development",
                            entities: [process.cwd() + '/src/models/**.entity.ts']
                        }
                    }
                }),
            ],
        })
            .overrideProvider(LogEndpointsService)
            .useValue(logEndpointService)
            .compile();

        app = moduleRef.createNestApplication();
        let c = await app.init();

        try {
            let data = await axios.post('http://localhost:8080/api/login', {
                apis: ['all'],
                username: 'test_user',
                password: '123!@#qweQWE'
            });
            token = data.data.access_token;
        } catch (e) {
            console.log(e)
        }
    });

    it(`/GET log/endpoint`, () => {
        return request(app.getHttpServer())
            .get('/log/endpoint')
            .auth(token, { type: 'bearer'})
            .expect(200)
            .expect(logEndpointService.findAll());
    });

    it(`/GET log/endpoint/user/:user_id`, () => {
        return request(app.getHttpServer())
            .get('/log/endpoint/user/1')
            .auth(token, { type: 'bearer'})
            .expect(200)
            .expect(logEndpointService.findAllByUserId());
    });

    it(`/GET log/endpoint/:id`, () => {
        return request(app.getHttpServer())
            .get('/log/endpoint/1')
            .auth(token, { type: 'bearer'})
            .expect(200)
            .expect(logEndpointService.findOne());
    });

    afterAll(async () => {
        await app.close();
    });
});