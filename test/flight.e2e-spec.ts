import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { FlightService } from '../src/resources/flight/flight.service';
import { INestApplication } from '@nestjs/common';
import { ConfigModule, ConfigService } from "@nestjs/config";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AuthModule } from "../src/auth/auth.module";
import axios from "axios";
import { SnakeNamingStrategy } from "typeorm-naming-strategies";
import { CONNECTION_LOCAL, CONNECTION_SHU } from "../src/constants/Connection.constants";
import { FlightModule } from "../src/resources/flight/flight.module";
import { UserModule } from "../src/resources/user/user.module";

describe('Flight', () => {
  let token = '';
  let app: INestApplication;
  let item = {
    isSeq: false,
    conn_time: null,
    tripId: null,
    id: 1,
    created: "2020-10-01T01:28:36.011Z",
    updated: "2020-10-01T01:28:36.011Z",
    flightNo: "create table flight",
    realFlightNo: null,
    operatingCarrier: null,
    marketedCarrier: null,
    statusName: null,
    lumoRisk: null,
    active: true,
    cancelled: false,
    diverted: false,
    departureDate: null,
    scheduledDeparture: null,
    estimatedDeparture: null,
    updatedEstimatedDeparture: null,
    scheduledDepartureDateLocal: null,
    scheduledDepartureTimeLocal: null,
    estimatedDepartureDateLocal: null,
    estimatedDepartureTimeLocal: null,
    scheduledArrival: null,
    estimatedArrival: null,
    updatedEstimatedArrival: null,
    scheduledArrivalDateLocal: null,
    scheduledArrivalTimeLocal: null,
    estimatedArrivalDateLocal: null,
    estimatedArrivalTimeLocal: null,
    origin: null,
    destination: null,
    diversionAirport: null,
    originTerminal: null,
    originGate: null,
    destinationTerminal: null,
    destinationGate: null,
    destinationBaggage: null,
    planeType: null,
    tailNo: null,
    lumoId: null,
    flightstatsId: null,
    firestoreId: null,
    firestorePassenger: null,
    firestoreData: null,
    initialLumoData: null,
    initialFlightStatsData: null
  };
  let flightService = {
    findAll: () => ({
      items: [item],
      meta: {
        totalItems: 1,
        itemCount: 1,
        itemsPerPage: 10,
        totalPages: 1,
        currentPage: 1
      }
    }),
    findOne: () => item
  };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          envFilePath: '.env',
          isGlobal: true
        }),
        AuthModule,
        UserModule,
        FlightModule,
        ConfigModule.forRoot({
          envFilePath: '.env',
          isGlobal: true
        }),
        TypeOrmModule.forRootAsync({
          imports: [
            ConfigModule
          ],
          inject: [ConfigService],
          name: CONNECTION_SHU,
          useFactory: (configService: ConfigService) => {
            return {
              name: CONNECTION_SHU,
              type: 'postgres',
              host: configService.get<string>("SHU_DB_HOST", "localhost"),
              port: configService.get<number>("SHU_DB_PORT", 5432),
              username: configService.get<string>("SHU_DB_USERNAME", "postgres"),
              password: configService.get<string>("SHU_DB_PASSWORD", "postgres"),
              database: configService.get<string>("SHU_DB_DATABASE_NAME", "shu_api"),
              synchronize: true,
              logging: configService.get<string>("NODE_ENV", "development") === "development",
              namingStrategy: new SnakeNamingStrategy(),
              entities: [process.cwd() + '/src/shu/dist/model/**.js'],
            }
          }
        }),
        TypeOrmModule.forRootAsync({
          imports: [
            ConfigModule
          ],
          inject: [ConfigService],
          name: 'local',
          useFactory: (configService: ConfigService) => {
            return {
              name: CONNECTION_LOCAL,
              type: 'postgres',
              host: configService.get<string>("DB_HOST", "localhost"),
              port: configService.get<number>("DB_PORT", 5432),
              username: configService.get<string>("DB_USERNAME", "postgres"),
              password: configService.get<string>("DB_PASSWORD", "postgres"),
              database: configService.get<string>("DB_DATABASE_NAME", "shu_api"),
              synchronize: true,
              namingStrategy: new SnakeNamingStrategy(),
              logging: configService.get<string>("NODE_ENV", "development") === "development",
              entities: [process.cwd() + '/src/models/**.entity.ts']
            }
          }
        }),
      ],
    })
      .overrideProvider(FlightService)
      .useValue(flightService)
      .compile();

    app = moduleRef.createNestApplication();
    let c = await app.init();

    try {
      let data = await axios.post('http://localhost:8080/api/login', {
        apis: ['all'],
        username: 'test_user',
        password: '123!@#qweQWE'
      });
      token = data.data.access_token;
    } catch (e) {
      console.log(e)
    }
  });

  it(`/GET flight`, () => {
    return request(app.getHttpServer())
      .get('/flight')
      .auth(token, { type: 'bearer'})
      .expect(200)
      .expect(flightService.findAll());
  });

  it(`/GET flight/:id`, () => {
    return request(app.getHttpServer())
      .get('/flight/1')
      .auth(token, { type: 'bearer'})
      .expect(200)
      .expect(flightService.findOne());
  });

  afterAll(async () => {
    await app.close();
  });
});