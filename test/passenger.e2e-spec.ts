import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { PassengerService } from '../src/resources/passenger/passenger.service';
import { INestApplication } from '@nestjs/common';
import { ConfigModule, ConfigService } from "@nestjs/config";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AuthModule } from "../src/auth/auth.module";
import axios from "axios";
import { SnakeNamingStrategy } from "typeorm-naming-strategies";
import { CONNECTION_LOCAL, CONNECTION_SHU } from "../src/constants/Connection.constants";
import { PassengerModule } from "../src/resources/passenger/passenger.module";
import { UserModule } from "../src/resources/user/user.module";

describe('Passenger', () => {
  let token = '';
  let app: INestApplication;
  let item = {
    id: 1,
    lastMessageDate: null,
    created: "2020-10-01T01:27:28.141Z",
    name: "test",
    firstName: "test",
    lastName: "test",
    email: "test@gmail.com",
    mobile: "37477777777",
    whatsapp: "37477777777",
    firestoreId: null,
    firestoreData: null,
    muteBot: false,
    confirmed_messaging: false,
    sent_24afterRegister: false,
    sent_24beforeFlight: false
  };
  let passengerService = {
    findAll: () => ({
      items: [item],
      meta: {
        totalItems: 1,
        itemCount: 1,
        itemsPerPage: 10,
        totalPages: 1,
        currentPage: 1
      }
    }),
    findOne: () => item
  };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          envFilePath: '.env',
          isGlobal: true
        }),
        AuthModule,
        UserModule,
        PassengerModule,
        ConfigModule.forRoot({
          envFilePath: '.env',
          isGlobal: true
        }),
        TypeOrmModule.forRootAsync({
          imports: [
            ConfigModule
          ],
          inject: [ConfigService],
          name: CONNECTION_SHU,
          useFactory: (configService: ConfigService) => {
            return {
              name: CONNECTION_SHU,
              type: 'postgres',
              host: configService.get<string>("SHU_DB_HOST", "localhost"),
              port: configService.get<number>("SHU_DB_PORT", 5432),
              username: configService.get<string>("SHU_DB_USERNAME", "postgres"),
              password: configService.get<string>("SHU_DB_PASSWORD", "postgres"),
              database: configService.get<string>("SHU_DB_DATABASE_NAME", "shu_api"),
              synchronize: true,
              logging: configService.get<string>("NODE_ENV", "development") === "development",
              namingStrategy: new SnakeNamingStrategy(),
              entities: [process.cwd() + '/src/shu/dist/model/**.js'],
            }
          }
        }),
        TypeOrmModule.forRootAsync({
          imports: [
            ConfigModule
          ],
          inject: [ConfigService],
          name: 'local',
          useFactory: (configService: ConfigService) => {
            return {
              name: CONNECTION_LOCAL,
              type: 'postgres',
              host: configService.get<string>("DB_HOST", "localhost"),
              port: configService.get<number>("DB_PORT", 5432),
              username: configService.get<string>("DB_USERNAME", "postgres"),
              password: configService.get<string>("DB_PASSWORD", "postgres"),
              database: configService.get<string>("DB_DATABASE_NAME", "shu_api"),
              synchronize: true,
              namingStrategy: new SnakeNamingStrategy(),
              logging: configService.get<string>("NODE_ENV", "development") === "development",
              entities: [process.cwd() + '/src/models/**.entity.ts']
            }
          }
        }),
      ],
    })
      .overrideProvider(PassengerService)
      .useValue(passengerService)
      .compile();

    app = moduleRef.createNestApplication();
    let c = await app.init();

    try {
      let data = await axios.post('http://localhost:8080/api/login', {
        apis: ['all'],
        username: 'test_user',
        password: '123!@#qweQWE'
      });
      token = data.data.access_token;
    } catch (e) {
      console.log(e)
    }
  });

  it(`/GET passenger`, () => {
    return request(app.getHttpServer())
      .get('/passenger')
      .auth(token, { type: 'bearer'})
      .expect(200)
      .expect(passengerService.findAll());
  });

  it(`/GET passenger/:id`, () => {
    return request(app.getHttpServer())
      .get('/passenger/1')
      .auth(token, { type: 'bearer'})
      .expect(200)
      .expect(passengerService.findOne());
  });

  afterAll(async () => {
    await app.close();
  });
});