import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { CompanyService } from '../src/resources/company/company.service';
import { INestApplication } from '@nestjs/common';
import { ConfigModule, ConfigService } from "@nestjs/config";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AuthModule } from "../src/auth/auth.module";
import axios from "axios";
import { SnakeNamingStrategy } from "typeorm-naming-strategies";
import { CONNECTION_LOCAL, CONNECTION_SHU } from "../src/constants/Connection.constants";
import { CompanyModule } from "../src/resources/company/company.module";
import { UserModule } from "../src/resources/user/user.module";

describe('Company', () => {
  let token = '';
  let app: INestApplication;
  let item = {
    id: 1,
    created: "2020-08-07T18:25:07.387Z",
    name: "GGLL Labss",
    credits: null,
    firestoreId: null,
    firestoreData: null
  };
  let companyService = {
    findAll: () => ({
      items: [item],
      meta: {
        totalItems: 1,
        itemCount: 1,
        itemsPerPage: 10,
        totalPages: 1,
        currentPage: 1
      }
    }),
    findOne: () => item
  };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          envFilePath: '.env',
          isGlobal: true
        }),
        AuthModule,
        UserModule,
        CompanyModule,
        ConfigModule.forRoot({
          envFilePath: '.env',
          isGlobal: true
        }),
        TypeOrmModule.forRootAsync({
          imports: [
            ConfigModule
          ],
          inject: [ConfigService],
          name: CONNECTION_SHU,
          useFactory: (configService: ConfigService) => {
            return {
              name: CONNECTION_SHU,
              type: 'postgres',
              host: configService.get<string>("SHU_DB_HOST", "localhost"),
              port: configService.get<number>("SHU_DB_PORT", 5432),
              username: configService.get<string>("SHU_DB_USERNAME", "postgres"),
              password: configService.get<string>("SHU_DB_PASSWORD", "postgres"),
              database: configService.get<string>("SHU_DB_DATABASE_NAME", "shu_api"),
              synchronize: true,
              logging: configService.get<string>("NODE_ENV", "development") === "development",
              namingStrategy: new SnakeNamingStrategy(),
              entities: [process.cwd() + '/src/shu/dist/model/**.js'],
            }
          }
        }),
        TypeOrmModule.forRootAsync({
          imports: [
            ConfigModule
          ],
          inject: [ConfigService],
          name: 'local',
          useFactory: (configService: ConfigService) => {
            return {
              name: CONNECTION_LOCAL,
              type: 'postgres',
              host: configService.get<string>("DB_HOST", "localhost"),
              port: configService.get<number>("DB_PORT", 5432),
              username: configService.get<string>("DB_USERNAME", "postgres"),
              password: configService.get<string>("DB_PASSWORD", "postgres"),
              database: configService.get<string>("DB_DATABASE_NAME", "shu_api"),
              synchronize: true,
              namingStrategy: new SnakeNamingStrategy(),
              logging: configService.get<string>("NODE_ENV", "development") === "development",
              entities: [process.cwd() + '/src/models/**.entity.ts']
            }
          }
        }),
      ],
    })
      .overrideProvider(CompanyService)
      .useValue(companyService)
      .compile();

    app = moduleRef.createNestApplication();
    let c = await app.init();

    try {
      let data = await axios.post('http://localhost:8080/api/login', {
        apis: ['all'],
        username: 'test_user',
        password: '123!@#qweQWE'
      });
      token = data.data.access_token;
    } catch (e) {
      console.log(e)
    }
  });

  it(`/GET company`, () => {
    return request(app.getHttpServer())
      .get('/company')
      .auth(token, { type: 'bearer'})
      .expect(200)
      .expect(companyService.findAll());
  });

  it(`/GET company/:id`, () => {
    return request(app.getHttpServer())
      .get('/company/1')
      .auth(token, { type: 'bearer'})
      .expect(200)
      .expect(companyService.findOne());
  });

  afterAll(async () => {
    await app.close();
  });
});