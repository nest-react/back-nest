import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { UserModule } from '../src/resources/user/user.module';
import { TokenService } from '../src/resources/token/token.service';
import { INestApplication } from '@nestjs/common';
import { ConfigModule, ConfigService } from "@nestjs/config";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AuthModule } from "../src/auth/auth.module";
import { TokenModule } from "../src/resources/token/token.module";
import axios from "axios";
import { SnakeNamingStrategy } from "typeorm-naming-strategies";
import { CONNECTION_LOCAL } from "../src/constants/Connection.constants";

describe('Token', () => {
    let token = '';
    let app: INestApplication;
    let tokenService = {
        findAll: () => ({
            items: [
                {
                    id: 1,
                    user_id: 1,
                    token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Ind3dyIsImlkIjozLCJpc0FkbWluIjp0cnVlLCJhcGlfbGlzdCI6WyJhaXJsaW5lIl0sImlhdCI6MTYwNzQwNzcxMX0.jmuGcbu0x9PuaUqmiXRTsVA2Vw12HaS2orN7PS0qAUo",
                    created_at: "2020-12-08T02:06:17.335Z",
                    name: "xx"
                }
            ],
            meta: {
                totalItems: 1,
                itemCount: 1,
                itemsPerPage: 10,
                totalPages: 1,
                currentPage: 1
            }
        }),
        findAllUsers: () => [
            {
                text: "text",
                value: 1
            }
        ]
    };

    beforeAll(async () => {
        const moduleRef = await Test.createTestingModule({
            imports: [
                ConfigModule.forRoot({
                    envFilePath: '.env',
                    isGlobal: true
                }),
                UserModule,
                TokenModule,
                AuthModule,
                ConfigModule.forRoot({
                    envFilePath: '.env',
                    isGlobal: true
                }),
                TypeOrmModule.forRootAsync({
                    imports: [
                        ConfigModule
                    ],
                    inject: [ConfigService],
                    name: 'local',
                    useFactory: (configService: ConfigService) => {
                        return {
                            name: CONNECTION_LOCAL,
                            type: 'postgres',
                            host: configService.get<string>("DB_HOST", "localhost"),
                            port: configService.get<number>("DB_PORT", 5432),
                            username: configService.get<string>("DB_USERNAME", "postgres"),
                            password: configService.get<string>("DB_PASSWORD", "postgres"),
                            database: configService.get<string>("DB_DATABASE_NAME", "shu_api"),
                            synchronize: true,
                            namingStrategy: new SnakeNamingStrategy(),
                            logging: configService.get<string>("NODE_ENV", "development") === "development",
                            entities: [process.cwd() + '/src/models/**.entity.ts']
                        }
                    }
                }),
            ],
        })
            .overrideProvider(TokenService)
            .useValue(tokenService)
            .compile();

        app = moduleRef.createNestApplication();
        let c = await app.init();

        try {
            let data = await axios.post('http://localhost:8080/api/login', {
                apis: ['all'],
                username: 'test_user',
                password: '123!@#qweQWE'
            });
            token = data.data.access_token;
        } catch (e) {
            console.log(e)
        }
    });

    it(`/GET tokens`, () => {
        return request(app.getHttpServer())
            .get('/token')
            .auth(token, { type: 'bearer'})
            .expect(200)
            .expect(tokenService.findAll());
    });

    it(`/GET token/users`, () => {
        return request(app.getHttpServer())
            .get('/token/users')
            .auth(token, { type: 'bearer'})
            .expect(200)
            .expect(tokenService.findAllUsers());
    });

    afterAll(async () => {
        await app.close();
    });
});