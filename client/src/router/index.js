import Vue from 'vue';
import VueRouter from 'vue-router';
import { authGuard } from '../auth';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    beforeEnter: authGuard,
    component: () => import('@/components/pages/home/Home.vue')
  },
  {
    path: '/airlines',
    name: 'airlines',
    beforeEnter: authGuard,
    component: () => import('@/components/pages/airline/Airlines.vue')
  },
  {
    path: '/tokens',
    name: 'tokens',
    beforeEnter: authGuard,
    component: () => import('@/components/pages/tokens/Tokens.vue')
  },
  {
    path: '/passengers',
    name: 'passengers',
    beforeEnter: authGuard,
    component: () => import('@/components/pages/passengers/Passengers.vue')
  },
  {
    path: '/company',
    name: 'company',
    beforeEnter: authGuard,
    component: () => import('@/components/pages/company/Company.vue')
  },
  {
    path: '/flight',
    name: 'flight',
    beforeEnter: authGuard,
    component: () => import('@/components/pages/flight/Flight.vue')
  },
  {
    path: '/trip',
    name: 'trip',
    beforeEnter: authGuard,
    component: () => import('@/components/pages/trip/Trip.vue')
  },
  {
    path: '/user-trip',
    name: 'user_trip',
    beforeEnter: authGuard,
    component: () => import('@/components/pages/userTrip/UserTrip.vue')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router
