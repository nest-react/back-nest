import axios from 'axios';
import Vue from 'vue';

const axiosApi = axios.create();
axiosApi.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
axiosApi.defaults.baseURL = process.env.VUE_APP_API_BASE_URL;

export const setAuthHeader = async () => {
    let sub = '';
    if (Vue.prototype.$auth) {
        let data = await Vue.prototype.$auth.getIdTokenClaims();

        if (data)
            sub = data.sub;
    }

    if (sub) {
        let data = await axiosApi.post('/api/auth0/login', {
            user_id: sub
        });

        if (data.data) {
            localStorage.setItem('is_admin', data.data.is_admin);
            axiosApi.defaults.headers.common['Authorization'] = `Bearer ${data.data.access_token}`;
        }
    }
}

export default axiosApi;