import Vue from 'vue'
import Vuex from 'vuex'
import axiosApi from '@/utils/axios'
import VueAxios from 'vue-axios'
import modules from './modules'

Vue.use(Vuex)
Vue.use(VueAxios, axiosApi)

export default new Vuex.Store({
  modules,
  plugins: [],
  strict: process.env.NODE_ENV !== 'production'
})