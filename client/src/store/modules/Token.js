import Vue from "vue";

const state = {
    save_error: false,
    token: '',
    users: [],
    api_list: [],
    tokens: {
        items: [],
        meta: {
            currentPage: 1,
            itemCount: 1,
            itemsPerPage: 10,
            totalItems: 1,
            totalPages: 1,
        }
    }
}

const mutations = {
    SET_TOKENS (state, tokens) {
        state.tokens = tokens;
    },
    SET_API_LIST (state, list) {
        state.api_list = list;
    },
    SET_SAVE_ERROR (state, error) {
        state.save_error = error;
    },
    SET_TOKEN (state, token) {
        state.token = token;
    },
    SET_USERS (state, users) {
        state.users = users;
    }
}

const actions = {
    tokens({ commit }, page = 1) {
        Vue.axios.get(`api/token?page=${page}`)
            .then(resp => {
                commit('SET_TOKENS', resp.data);
            });
    },
    apiList({ commit }) {
        Vue.axios.get('api/user/api/list')
            .then(resp => {
                commit('SET_API_LIST', resp.data);
            });
    },
    async deleteToken({ dispatch }, id) {
        await Vue.axios.delete(`api/token/${id}`);
        dispatch('tokens');
    },
    async saveToken({ commit, dispatch }, data) {
        try {
            let token_data = await Vue.axios.post('api/token', data);

            commit('SET_SAVE_ERROR', false);
            commit('SET_TOKEN', token_data.data.access_token)
            dispatch('tokens');
        } catch (e) {
            commit('SET_SAVE_ERROR', true);
        }
    },
    changeTokenData({ commit }, token) {
        commit('SET_TOKEN', token);
    },
    async tokenUsers({ commit }) {
        let data = await Vue.axios.get('api/token/users');

        commit('SET_USERS', data.data);
    }
}

const getters = {

}

export default {
    state,
    mutations,
    actions,
    getters
}
