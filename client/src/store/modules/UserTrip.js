import Vue from "vue";

const state = {
    user_trips: {
        items: [],
        meta: {
            currentPage: 1,
            itemCount: 1,
            itemsPerPage: 10,
            totalItems: 1,
            totalPages: 1,
        }
    }
}

const mutations = {
    SET_USER_TRIPS (state, user_trips) {
        state.user_trips = user_trips;
    },
}

const actions = {
    userTrips({ commit }, page = 1) {
        Vue.axios.get(`api/user-trip?page=${page}`)
            .then(resp => {
                commit('SET_USER_TRIPS', resp.data);
            });
    },
}

const getters = {

}

export default {
    state,
    mutations,
    actions,
    getters
}
