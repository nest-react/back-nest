import Vue from "vue";

const state = {
    save_error: false,
    airlines: {
        items: [],
        meta: {
            currentPage: 1,
            itemCount: 1,
            itemsPerPage: 10,
            totalItems: 1,
            totalPages: 1,
        }
    }
}

const mutations = {
    SET_AIRLINES (state, airlines) {
        state.airlines = airlines;
    },
    SET_SAVE_ERROR (state, error) {
        state.save_error = error;
    }
}

const actions = {
    airlines({ commit }, page = 1) {
        Vue.axios.get(`api/airline?page=${page}`)
            .then(resp => {
                commit('SET_AIRLINES', resp.data);
            });
    },
    async deleteAirline({ dispatch }, id) {
        await Vue.axios.delete(`api/airline/${id}`);
        dispatch('airlines');
    },
    async updateOrSaveAirline({ commit, dispatch }, data) {
        try {
            if (data.id)
                await Vue.axios.put(`api/airline/${data.id}`, data);
            else
                await Vue.axios.post('api/airline', data);

            commit('SET_SAVE_ERROR', false);
            dispatch('airlines');
        } catch (e) {
            commit('SET_SAVE_ERROR', true);
        }
    }
}

const getters = {

}

export default {
    state,
    mutations,
    actions,
    getters
}
