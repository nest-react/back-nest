import Vue from "vue";

const state = {
    save_error: false,
    users: {
        items: [],
        meta: {
            currentPage: 1,
            itemCount: 1,
            itemsPerPage: 10,
            totalItems: 1,
            totalPages: 1,
        }
    }
}

const mutations = {
    SET_USERS (state, users) {
        state.users = users;
    },
    SET_SAVE_ERROR (state, error) {
        state.save_error = error;
    }
}

const actions = {
    users({ commit }, page = 1) {
        Vue.axios.get(`api/user?page=${page}`)
            .then(resp => {
                commit('SET_USERS', resp.data);
            });
    },
    async deleteUser({ dispatch }, id) {
        await Vue.axios.delete(`api/user/${id}`);
        dispatch('users');
    },
    async updateOrSaveUser({ commit, dispatch }, data) {
        try {
            if (data.id)
                await Vue.axios.put(`api/user/${data.id}`, data);
            else
                await Vue.axios.post('api/user', data);

            commit('SET_SAVE_ERROR', false);
            dispatch('users');
        } catch (e) {
            commit('SET_SAVE_ERROR', true);
        }
    }
}

const getters = {

}

export default {
    state,
    mutations,
    actions,
    getters
}
