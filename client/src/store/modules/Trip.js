import Vue from "vue";

const state = {
    trips: {
        items: [],
        meta: {
            currentPage: 1,
            itemCount: 1,
            itemsPerPage: 10,
            totalItems: 1,
            totalPages: 1,
        }
    }
}

const mutations = {
    SET_TRIPS (state, trips) {
        state.trips = trips;
    },
}

const actions = {
    trips({ commit }, page = 1) {
        Vue.axios.get(`api/trip?page=${page}`)
            .then(resp => {
                commit('SET_TRIPS', resp.data);
            });
    },
}

const getters = {

}

export default {
    state,
    mutations,
    actions,
    getters
}
