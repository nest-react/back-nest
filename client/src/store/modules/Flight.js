import Vue from "vue";

const state = {
    flights: {
        items: [],
        meta: {
            currentPage: 1,
            itemCount: 1,
            itemsPerPage: 10,
            totalItems: 1,
            totalPages: 1,
        }
    }
}

const mutations = {
    SET_FLIGHTS (state, flights) {
        state.flights = flights;
    },
}

const actions = {
    flights({ commit }, page = 1) {
        Vue.axios.get(`api/flight?page=${page}`)
            .then(resp => {
                commit('SET_FLIGHTS', resp.data);
            });
    },
}

const getters = {

}

export default {
    state,
    mutations,
    actions,
    getters
}
