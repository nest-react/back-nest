import Vue from "vue";

const state = {
    companies: {
        items: [],
        meta: {
            currentPage: 1,
            itemCount: 1,
            itemsPerPage: 10,
            totalItems: 1,
            totalPages: 1,
        }
    }
}

const mutations = {
    SET_COMPANY (state, companies) {
        state.companies = companies;
    },
}

const actions = {
    companies({ commit }, page = 1) {
        Vue.axios.get(`api/company?page=${page}`)
            .then(resp => {
                commit('SET_COMPANY', resp.data);
            });
    },
}

const getters = {

}

export default {
    state,
    mutations,
    actions,
    getters
}
