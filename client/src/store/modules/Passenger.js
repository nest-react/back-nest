import Vue from "vue";

const state = {
    passengers: {
        items: [],
        meta: {
            currentPage: 1,
            itemCount: 1,
            itemsPerPage: 10,
            totalItems: 1,
            totalPages: 1,
        }
    }
}

const mutations = {
    SET_PASSENGERS (state, passengers) {
        state.passengers = passengers;
    },
}

const actions = {
    passengers({ commit }, page = 1) {
        Vue.axios.get(`api/passenger?page=${page}`)
            .then(resp => {
                commit('SET_PASSENGERS', resp.data);
            });
    },
}

const getters = {

}

export default {
    state,
    mutations,
    actions,
    getters
}
