const SnakeNamingStrategy = require("typeorm-naming-strategies")
    .SnakeNamingStrategy;

const dotenv = require('dotenv');

dotenv.config();

module.exports = {
    type: "postgres",
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE_NAME,
    synchronize: true,
    logging: false,
    namingStrategy: new SnakeNamingStrategy(),
    entities: [__dirname + '/src/models/**.entity.ts'],
    migrations: ["src/db/migration/**/*.ts"],
    seeds: ['src/db/seeds/**.seed.ts'],
    cli: {
        entitiesDir: "src/model",
        migrationsDir: "src/db/migration",
        seedsDir: "src/db/seeds"
    },
};
