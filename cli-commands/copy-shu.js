let copy = require("recursive-copy");

let options = {
  overwrite: true
};

let args = process.argv.slice(2);
let dir_name = 'shu';

if (args.length && args[0].indexOf('path=') > -1)
  dir_name = args[0].slice(5);

copy(`${process.cwd()}/../${dir_name}/dist`, `${process.cwd()}/src/shu/dist`, options)
  .catch(function(error) {
    return console.error('Copy failed: ' + error);
  });

copy(`${process.cwd()}/../${dir_name}/node_modules`, `${process.cwd()}/src/shu/node_modules`, options)
  .catch(function(error) {
    return console.error('Copy failed: ' + error);
  });
