import { Factory, Seeder } from "typeorm-seeding";
import { Connection } from "typeorm";
import { UserEntity } from "../../models/user.entity";

export default class AddTestUser implements Seeder {
    public async run(factory: Factory, connection: Connection): Promise<any> {
        const test_user = new UserEntity();

        test_user.name = 'test_user';
        test_user.is_admin = true;

        await connection.getRepository(UserEntity)
            .save(test_user);
    }
}