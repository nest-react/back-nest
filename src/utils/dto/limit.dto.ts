"use strict";

import { IsOptional } from "class-validator";

export class LimitDto {
    @IsOptional()
    limit?: number;

    @IsOptional()
    page?: number;
}