import { Api } from '../../enum/api.enum';
import {
    ValidatorConstraint,
    ValidatorConstraintInterface,
    ValidationArguments
} from "class-validator";

@ValidatorConstraint({ name: "isCorrectApiList", async: false })
export class IsCorrectApiList implements ValidatorConstraintInterface {
    validate(api_list: string[], args: ValidationArguments) {
        if (!api_list)
            return false;

        for (let api of api_list)
            if (!Api[api.toUpperCase()])
                return false;

        return true;
    }

    defaultMessage(args: ValidationArguments) {
        return "apis list is incorrect";
    }
}