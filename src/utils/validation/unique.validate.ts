import {
    ValidatorConstraint,
    ValidatorConstraintInterface,
    ValidationArguments
} from "class-validator";

import { getConnection } from "typeorm";

@ValidatorConstraint({ name: "isUnique", async: false })
export class IsUnique implements ValidatorConstraintInterface {
    async validate(text: string, args: ValidationArguments) {
        let is_update = args.targetName.includes('Update');
        let where = {};
        where[args.constraints[1]] = text;

        let item = await getConnection(args.constraints[2]).getRepository(args.constraints[0]).findOne({where});

        return (is_update && item && args.object['id'] == item['id']) || item == undefined;
    }

    defaultMessage(args: ValidationArguments) {
        return "($value) exists";
    }
}