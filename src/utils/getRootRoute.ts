import {Route} from '../enum/route.enum';

export function rootRoute(route: string): Route {
    let normalized_route = route.slice(1)
        .split(':')[0];

    let split = normalized_route.split('/')
        .join('_');

    if (split[split.length - 1] == '_') {
        split = split.slice(0 , -1);
    }

    let root_route = split.toUpperCase();

    return Route[root_route] ? Route[root_route] : Route.ND;
}