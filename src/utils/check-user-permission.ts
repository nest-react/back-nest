import {Route} from "../enum/route.enum";
import { getConnection } from "typeorm";
import { TokenEntity } from "../models/token.entity";
import { CONNECTION_LOCAL } from "../constants/Connection.constants";

export async function checkPermission(request: any, route: Route): Promise<boolean> {
    if ([Route.ND, Route.LOGIN].includes(route)) {
        return true;
    }

    let token = request.headers.authorization.slice(7);

    let tokenEntityRepository = getConnection(CONNECTION_LOCAL).getRepository(TokenEntity);
    let item = await tokenEntityRepository.findOne({
        where: {
            token
        }
    });

    if (!item || item.is_expired) {
        return false;
    }

    if (request.user.hasOwnProperty('api_list'))
        for (let api of request.user.api_list)
            if (api === route)
                return true;

    return false;
}