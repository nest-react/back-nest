import {
    CallHandler,
    ExecutionContext,
    UnauthorizedException,
    Injectable,
    NestInterceptor
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { LogEndpointEntity } from "../models/logEndpoin.entity";
import { getConnection } from "typeorm";
import { rootRoute } from '../utils/getRootRoute';
import { checkPermission } from '../utils/check-user-permission';
import { CONNECTION_LOCAL } from "../constants/Connection.constants";

@Injectable()
export class LoggingInterceptor implements NestInterceptor {
  async intercept(context: ExecutionContext, next: CallHandler): Promise<Observable<any>> {
    const now = Date.now();
    let request = context.switchToHttp().getRequest();
    let root_route = rootRoute(request.route.path);

    if (!await checkPermission(request, root_route))
        throw new UnauthorizedException();

    return next
        .handle()
        .pipe(
            tap(async () => {
                let time = Date.now() - now;
                let logEndpoint = getConnection(CONNECTION_LOCAL).getRepository(LogEndpointEntity);
                let log = new LogEndpointEntity();

                log.user = request.user.id;
                log.time = time;
                log.type = root_route;
                log.info = {
                    route: request.url,
                    method: request.method
                };

                await logEndpoint.save(log);
            }),
        );
  }
}