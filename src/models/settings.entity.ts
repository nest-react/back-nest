"use strict";

import {
  Entity,
  Column,
  PrimaryGeneratedColumn
} from 'typeorm';

@Entity({
  name: 'settings'
})
export class SettingsEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    unique: true
  })
  name: string;

  @Column()
  value: string;
}
