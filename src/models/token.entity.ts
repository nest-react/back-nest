"use strict";

import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    ManyToOne,
    JoinColumn
} from 'typeorm';
import { UserEntity } from "./user.entity";

@Entity({
    name: 'tokens'
})
export class TokenEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    token: string;

    @ManyToOne(type => UserEntity)
    @JoinColumn()
    user: UserEntity;

    @Column({
        default: false
    })
    is_expired: boolean;

    @Column({
        type: "timestamp",
        default: () => "CURRENT_TIMESTAMP"
    })
    created_at: Date
}
