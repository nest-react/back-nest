"use strict";

import {Entity, Column, PrimaryGeneratedColumn} from 'typeorm';

@Entity({
    name: 'airlines'
})
export class AirlineEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column({
        unique: true
    })
    code: string;

    @Column()
    phone: string;

    @Column({
        type: "timestamp",
        default: () => "CURRENT_TIMESTAMP"
    })
    created_at: Date
}
