"use strict";

import {
    Entity,
    Column,
    PrimaryGeneratedColumn
} from 'typeorm';

@Entity({
    name: 'users'
})
export class UserEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        unique: true
    })
    user_id: string;

    @Column()
    name: string;

    @Column()
    email: string;

    @Column()
    picture: string;

    @Column({
        default: false
    })
    is_admin: boolean;

    @Column({
        type: "timestamp",
        default: () => "CURRENT_TIMESTAMP"
    })
    created_at: Date
}
