"use strict";

import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    ManyToOne,
    JoinColumn
} from 'typeorm';
import { Route } from "../enum/route.enum";
import { UserEntity } from "./user.entity";

@Entity({
    name: 'log_endpoint'
})
export class LogEndpointEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => UserEntity)
    @JoinColumn()
    user: UserEntity;

    @Column({
        type: "enum",
        enum: Route
    })
    type: Route;

    @Column('json')
    info: object;

    @Column()
    time: number;

    @Column({
        type: "timestamp",
        default: () => "CURRENT_TIMESTAMP"
    })
    created_at: Date
}