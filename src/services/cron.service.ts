import {
  HttpService,
  Injectable
} from '@nestjs/common';
import {
  Cron,
  CronExpression
} from '@nestjs/schedule';
import { CONNECTION_LOCAL } from '../constants/Connection.constants';
import { AUTH0_TOKEN } from '../constants/Settings.constants';
import { getConnection, Repository } from 'typeorm';
import { UserEntity } from '../models/user.entity';
import { SettingsEntity } from '../models/settings.entity';

@Injectable()
export class CronService {
  constructor(private httpService: HttpService) {}

  private userRepository: Repository<UserEntity>;
  private settingsRepository: Repository<SettingsEntity>;
  private headers = {
    authorization: ''
  }

  @Cron(CronExpression.EVERY_10_MINUTES)
  async Auth0GetUsersCron() {
    this.settingsRepository = getConnection(CONNECTION_LOCAL).getRepository(SettingsEntity);
    this.userRepository = getConnection(CONNECTION_LOCAL).getRepository(UserEntity);

    await this.setToken();

    let roles = await this.httpService.get(`https://${process.env.AUTH0_DOMAIN}.auth0.com/api/v2/roles`, {
      headers: this.headers
    })
      .pipe()
      .toPromise();

    for (let role of roles.data) {
      let is_admin = role.name.toLowerCase() === 'admin';

      await this.recGetUsers(role.id, is_admin);
    }
  }

  private async recGetUsers(role_id: string, is_admin: boolean, page: number = 0) {
    let data = await this.getRoleUsers(role_id, page);

    let user_ids = data.users.map((item) => item.user_id);

    let db_users = await this.userRepository
      .createQueryBuilder('user')
      .where("user.user_id IN(:...user_ids)", {
        user_ids: user_ids
      })
      .getMany();

    let not_exists_users = [];
    let existing_user_ids = db_users.map(item => item.user_id);
    let update_data = [];

    for (let user of data.users) {
      if (!existing_user_ids.includes(user.user_id)) {
        not_exists_users.push(user);
      } else {
        let item = db_users.find(item => item.user_id === user.user_id);

        if (item.is_admin !== is_admin)
          update_data.push({
            ...item,
            ...{
              is_admin
            }
          });
      }
    }

    for (let user of update_data)
      await this.userRepository.save(user);

    if (not_exists_users.length > 0) {
      not_exists_users = not_exists_users.map(item => {
        item.is_admin = is_admin

        return item;
      });
    }

    await this.userRepository.save(not_exists_users);

    if (data.users.length + data.start < data.total)
      await this.recGetUsers(role_id, is_admin, page + 1);
  }

  private async getRoleUsers(role_id: string, page: number): Promise<any> {
    let users = await this.httpService.get(`https://${process.env.AUTH0_DOMAIN}.auth0.com/api/v2/roles/${role_id}/users?include_totals=true&page=${page}`, {
      headers: this.headers
    })
      .pipe()
      .toPromise();

    return users.data;
  }

  private async setToken(): Promise<null> {
    let token = await this.settingsRepository.findOne({
      where: {
        name: AUTH0_TOKEN
      }
    });

    if (!token) {
      await this.saveToken();
    } else {
      let token_data = JSON.parse(token.value);
      let created_at = new Date(token_data.created_at);
      let now = new Date().getSeconds();
      let end_data = created_at.setSeconds( created_at.getSeconds() + token_data.expires_in - 120);

      if (now >= end_data) {
        await this.saveToken();
      } else {
        this.headers.authorization = `Bearer ${token_data.access_token}`;
      }
    }

    return null;
  }

  private async saveToken(): Promise<void> {
    let new_token = await this.getToken();

    let data = {
      access_token: new_token.access_token,
      expires_in: new_token.expires_in,
      created_at: new Date(),
    };

    await this.settingsRepository.save({
      name: AUTH0_TOKEN,
      value: JSON.stringify(data)
    });

    this.headers.authorization = `Bearer ${new_token.access_token}`;
  }

  private async getToken() {
    let token = await this.httpService.post(`https://${process.env.AUTH0_DOMAIN}.auth0.com/oauth/token`, {
      client_id: process.env.AUTH0_CLIENT_ID,
      client_secret: process.env.AUTH0_CLIENT_SECRET,
      audience: `https://${process.env.AUTH0_DOMAIN}.auth0.com/api/v2/`,
      grant_type: 'client_credentials'
    })
      .pipe()
      .toPromise();

    return token.data;
  }
}