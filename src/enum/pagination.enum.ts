export enum Pagination {
    limit = 10,
    page = 1
}