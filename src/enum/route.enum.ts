export enum Route {
    USER = 'user',
    AIRLINE = 'airline',
    LOGIN = 'login',
    LOGIN_FRONT = 'login_front',
    ND = 'nd',
    LOG_ENDPOINT = 'log_endpoint',
    TOKEN = 'token',
    PASSENGER = 'passenger',
}
