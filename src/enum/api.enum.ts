export enum Api {
    ALL = 'all',
    USER = 'user',
    AIRLINE = 'airline',
    LOG_ENDPOINT = 'log_endpoint',
    TOKEN = 'token',
    PASSENGER = 'passenger',
}