import { Module } from '@nestjs/common';
import { PassengerService } from './passenger.service';
import { PassengerController } from './passenger.controller';
import {TypeOrmModule} from "@nestjs/typeorm";
import { CONNECTION_SHU } from "../../constants/Connection.constants";
import { JwtStrategy } from "../../auth/jwt.strategy";
import User from '../../shu/dist/model/User';

@Module({
  imports: [
    TypeOrmModule.forFeature([
        User
    ], CONNECTION_SHU)
  ],
  controllers: [PassengerController],
  providers: [
      PassengerService,
      JwtStrategy
  ]
})
export class PassengerModule {}
