import {Injectable, NotFoundException} from '@nestjs/common';
import { CreatePassengerDto } from './dto/create-passenger.dto';
import { UpdatePassengerDto } from './dto/update-passenger.dto';
import { InjectRepository } from "@nestjs/typeorm";
import { CONNECTION_SHU } from "../../constants/Connection.constants";
import { Repository } from "typeorm";
import User from '../../shu/dist/model/User';
import { IPaginationOptions, paginate, Pagination } from "nestjs-typeorm-paginate";

@Injectable()
export class PassengerService {
  constructor(@InjectRepository(User, CONNECTION_SHU) private userRepository: Repository<User>) {}

  async create(createPassengerDto: CreatePassengerDto) {
    await this.userRepository.save(new User(createPassengerDto));

    return {
      message: 'created successfully'
    };
  }

  async findAll(options: IPaginationOptions): Promise<Pagination<User>> {
    const queryBuilder = this.userRepository;

    return paginate<User>(queryBuilder, options);
  }

  async findOne(id: number): Promise<User> {
    let user = await this.userRepository.findOne(id);

    if (!user)
      throw new NotFoundException();

    return user;
  }

  async update(id: number, updatePassengerDto: UpdatePassengerDto) {
    let user = await this.userRepository.findOne(id);

    if (!user)
      throw new NotFoundException();

    await this.userRepository.save({
      ...user,
      ...updatePassengerDto
    });

    return {
      message: 'updated successfully'
    };
  }

  async remove(id: number) {
    let deleted = await this.userRepository.delete(id);

    if (!deleted.affected)
      throw new NotFoundException();

    return {
      message: 'deleted successfully'
    };
  }
}
