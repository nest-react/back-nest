import {
    IsNotEmpty,
    Validate,
    IsPhoneNumber,
    IsEmail,
    MinLength
} from "class-validator";
import { IsUnique } from '../../../utils/validation/unique.validate';
import User  from "../../../shu/dist/model/User";
import { CONNECTION_SHU } from "../../../constants/Connection.constants";

export class CreatePassengerDto {
    @IsNotEmpty()
    @MinLength(2)
    name: string

    @IsNotEmpty()
    @MinLength(2)
    firstName: string

    @IsNotEmpty()
    @MinLength(2)
    lastName: string

    @IsNotEmpty()
    @IsEmail()
    email: string;

    @IsNotEmpty()
    @IsPhoneNumber(null)
    @Validate(IsUnique, [User, 'mobile', CONNECTION_SHU])
    mobile: string;

    @IsNotEmpty()
    @IsPhoneNumber(null)
    whatsapp: string;

    firestoreId: string;

    firestoreData: string;

    muteBot: boolean;

    confirmed_messaging: boolean;

    sent_24afterRegister: boolean;

    sent_24beforeFlight: boolean;
}