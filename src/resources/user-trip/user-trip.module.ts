import { Module } from '@nestjs/common';
import { UserTripService } from './user-trip.service';
import { UserTripController } from './user-trip.controller';
import { TypeOrmModule } from "@nestjs/typeorm";
import { CONNECTION_SHU } from "../../constants/Connection.constants";
import { JwtStrategy } from "../../auth/jwt.strategy";
import { UserTrip } from '../../shu/dist/model/UserTrip';
import User from "../../shu/dist/model/User";


@Module({
  imports: [
      TypeOrmModule.forFeature([
          UserTrip,
          User
      ], CONNECTION_SHU)
  ],
  controllers: [UserTripController],
  providers: [
      UserTripService,
    JwtStrategy
  ]
})
export class UserTripModule {}
