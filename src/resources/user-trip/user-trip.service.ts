import {BadRequestException, Injectable, NotFoundException} from '@nestjs/common';
import { CreateUserTripDto } from './dto/create-user-trip.dto';
import { UpdateUserTripDto } from './dto/update-user-trip.dto';
import {IPaginationOptions, paginate, Pagination} from "nestjs-typeorm-paginate";
import { InjectRepository } from "@nestjs/typeorm";
import { CONNECTION_SHU } from "../../constants/Connection.constants";
import { Repository } from "typeorm";
import { UserTrip } from "../../shu/dist/model/UserTrip";
import User from "../../shu/dist/model/User";

@Injectable()
export class UserTripService {
  constructor(
      @InjectRepository(UserTrip, CONNECTION_SHU) private userTripRepository: Repository<UserTrip>,
      @InjectRepository(User, CONNECTION_SHU) private userRepository: Repository<User>,
  ) {}

  async create(createUserTripDto: CreateUserTripDto) {
    let user = await this.userTripRepository.findOne(createUserTripDto.user_id);

    if (!user)
      throw new BadRequestException("user doesn't exists");

    await this.userTripRepository.save(new UserTrip({
      user_id: createUserTripDto.user_id,
      trip_id: createUserTripDto.trip_id,
    }));

    return {
      message: 'created successfully'
    };
  }

  async findAll(options: IPaginationOptions): Promise<Pagination<UserTrip>> {
    const queryBuilder = this.userTripRepository;

    return paginate<UserTrip>(queryBuilder, options);
  }

  async findOne(id: number): Promise<UserTrip> {
    const item = await this.userTripRepository.findOne(id);

    if (!item)
      throw new BadRequestException();

    return item;
  }

  async update(id: number, updateUserTripDto: UpdateUserTripDto) {
    let user = await this.userTripRepository.findOne(id);

    if (!user)
      throw new BadRequestException("user doesn't exists");

    const item = await this.userTripRepository.findOne(id);

    if (!item)
      throw new BadRequestException();

    await this.userTripRepository.save({
      user_id: updateUserTripDto.user_id,
      trip_id: updateUserTripDto.trip_id
    });

    return {
      message: 'updated successfully'
    }
  }

  async remove(id: number) {
    let item = await this.userTripRepository.delete(id);

    if (!item.affected)
      throw new NotFoundException();

    return {
      message: 'deleted successfully'
    };
  }
}
