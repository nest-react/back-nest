import {IsNotEmpty, Min} from "class-validator";

export class CreateUserTripDto {
    @IsNotEmpty()
    @Min(1)
    user_id: number

    @IsNotEmpty()
    @Min(1)
    trip_id: number
}
