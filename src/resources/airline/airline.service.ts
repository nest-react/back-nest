import {Injectable, NotFoundException} from '@nestjs/common';
import { CreateAirlineDto } from './dto/create-airline.dto';
import { UpdateAirlineDto } from './dto/update-airline.dto';
import { InjectRepository } from "@nestjs/typeorm";
import { AirlineEntity } from "../../models/airline.entity";
import { Repository } from "typeorm";
import { IPaginationOptions, paginate, Pagination } from "nestjs-typeorm-paginate";
import { CONNECTION_LOCAL } from '../../constants/Connection.constants';

@Injectable()
export class AirlineService {
  constructor(
      @InjectRepository(AirlineEntity, CONNECTION_LOCAL) private airlineRepository: Repository<AirlineEntity>
  ) {}

  async create(createAirlineDto: CreateAirlineDto) {
    let airline = new AirlineEntity();

    airline.code = createAirlineDto.code;
    airline.name = createAirlineDto.name;
    airline.phone = createAirlineDto.phone;

    await this.airlineRepository.save(airline);

    return {
      message: 'created successfully'
    };
  }

  async findAll(options: IPaginationOptions): Promise<Pagination<AirlineEntity>> {
    const queryBuilder = this.airlineRepository;

    return paginate<AirlineEntity>(queryBuilder, options);
  }

  async findOne(code: string) {
    let airline = await this.airlineRepository.findOne({
      where: {
        code
      }
    });

    if (!airline)
      throw new NotFoundException();

    return airline;
  }

  async update( id: number, updateAirlineDto: UpdateAirlineDto) {
    let airline = await this.airlineRepository.findOne(id);

    if (!airline)
      throw new NotFoundException();

    await this.airlineRepository.save({
      ...airline,
      ...updateAirlineDto
    });

    return {
      message: 'updated successfully'
    };
  }

  async remove(id: number) {
    let airline = await this.airlineRepository.delete(id);

    if (!airline.affected)
      throw new NotFoundException();

    return {
      message: 'deleted successfully'
    };
  }
}
