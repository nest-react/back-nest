"use strict";

import { IsUnique } from '../../../utils/validation/unique.validate';
import { AirlineEntity } from '../../../models/airline.entity';
import { CONNECTION_LOCAL } from "../../../constants/Connection.constants";
import {
    IsNotEmpty,
    Length,
    Validate,
    IsPhoneNumber
} from "class-validator";

export class CreateAirlineDto {
    @IsNotEmpty()
    @Length(2)
    @Validate(IsUnique, [AirlineEntity, 'code', CONNECTION_LOCAL])
    code: string;

    @IsNotEmpty()
    @Length(2)
    name: string;

    @IsPhoneNumber(null)
    phone: string;
}
