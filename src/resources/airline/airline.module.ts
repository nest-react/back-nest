import { Module } from '@nestjs/common';
import { AirlineService } from './airline.service';
import { AirlineController } from './airline.controller';
import { TypeOrmModule } from "@nestjs/typeorm";
import { AirlineEntity } from '../../models/airline.entity';
import { CONNECTION_LOCAL } from '../../constants/Connection.constants';
import { JwtStrategy } from "../../auth/jwt.strategy";

@Module({
  imports: [
      TypeOrmModule.forFeature([
          AirlineEntity
      ], CONNECTION_LOCAL)
  ],
  controllers: [AirlineController],
  providers: [
      AirlineService,
      JwtStrategy
  ]
})
export class AirlineModule {}
