import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { LogEndpointEntity } from '../../models/logEndpoin.entity';
import { Repository } from 'typeorm';
import {
  IPaginationOptions,
  paginate, paginateRaw,
  Pagination
} from 'nestjs-typeorm-paginate';
import { CONNECTION_LOCAL } from '../../constants/Connection.constants';

@Injectable()
export class LogEndpointsService {
  constructor(
      @InjectRepository(LogEndpointEntity, CONNECTION_LOCAL) private logEndpointRepository: Repository<LogEndpointEntity>
  ) {}

  async findAll(options: IPaginationOptions): Promise<Pagination<LogEndpointEntity>> {
    const queryBuilder = this.logEndpointRepository;

    return paginate<LogEndpointEntity>(queryBuilder, options);
  }

  async findAllByUserId(user_id: number, options: IPaginationOptions): Promise<Pagination<LogEndpointEntity>> {
    const queryBuilder = this.logEndpointRepository
        .createQueryBuilder()
        .select('id, user_id, type, time, info, created_at')
        .where({
          user: user_id
        });

    return paginateRaw(queryBuilder, options);
  }

  async findOne(id: number) {
    let item = await this.logEndpointRepository.findOne(id);

    if (!item)
      throw new NotFoundException();

    return item;
  }

  async remove(id: number) {
    let item = await this.logEndpointRepository.delete(id);

    if (!item.affected)
      throw new NotFoundException();

    return {
      message: 'deleted successfully'
    };
  }
}
