import { Module } from '@nestjs/common';
import { LogEndpointsService } from './log-endpoints.service';
import { LogEndpointsController } from './log-endpoints.controller';
import { TypeOrmModule } from "@nestjs/typeorm";
import { LogEndpointEntity } from '../../models/logEndpoin.entity';
import { CONNECTION_LOCAL } from '../../constants/Connection.constants';
import { JwtStrategy } from "../../auth/jwt.strategy";

@Module({
  imports: [
      TypeOrmModule.forFeature([
          LogEndpointEntity
      ], CONNECTION_LOCAL)
  ],
  controllers: [LogEndpointsController],
  providers: [
      LogEndpointsService,
      JwtStrategy
  ]
})
export class LogEndpointsModule {}
