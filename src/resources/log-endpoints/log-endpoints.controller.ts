import {
  Controller,
  Delete,
  Get,
  Param,
  Query,
  UseGuards
} from '@nestjs/common';
import { LogEndpointsService } from './log-endpoints.service';
import { JwtAuthGuard } from "../../auth/jwt-auth.guard";
import { RolesGuard } from "../../roles/roles.guard";
import { Roles } from "../../roles/roles.decorator";
import { Role } from "../../enum/role.enum";
import { Pagination } from "../../enum/pagination.enum";
import { LimitDto } from "../../utils/dto/limit.dto";

@Controller('log/endpoint')
export class LogEndpointsController {
  constructor(private readonly logEndpointsService: LogEndpointsService) {}

  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @Get()
  findAll(
      @Query('page') page: number = Pagination.page,
      @Query('limit') limit: number = Pagination.limit,
      limitDto: LimitDto
  ) {
    return this.logEndpointsService.findAll({ limit, page });
  }

  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @Get('user/:user_id')
  findAllByUserId(
      @Param('user_id') user_id: string,
      @Query('page') page: number = Pagination.page,
      @Query('limit') limit: number = Pagination.limit,
      limitDto: LimitDto
  ){
    return this.logEndpointsService.findAllByUserId(+user_id, { limit, page });
  }

  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.logEndpointsService.findOne(+id);
  }

  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.logEndpointsService.remove(+id);
  }
}
