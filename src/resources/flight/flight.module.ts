import { Module } from '@nestjs/common';
import { FlightService } from './flight.service';
import { FlightController } from './flight.controller';
import { TypeOrmModule } from "@nestjs/typeorm";
import { CONNECTION_SHU } from '../../constants/Connection.constants';
import { JwtStrategy } from "../../auth/jwt.strategy";
import Flight from '../../shu/dist/model/Flight';

@Module({
  imports: [
      TypeOrmModule.forFeature([
          Flight
      ], CONNECTION_SHU)
  ],
  controllers: [FlightController],
  providers: [
      FlightService,
      JwtStrategy
  ]
})
export class FlightModule {}
