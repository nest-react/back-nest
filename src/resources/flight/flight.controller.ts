import {Controller, Get, Param, Query, UseGuards} from '@nestjs/common';
import { FlightService } from './flight.service';
import { Pagination } from "../../enum/pagination.enum";
import { LimitDto } from "../../utils/dto/limit.dto";
import { JwtAuthGuard } from "../../auth/jwt-auth.guard";
import { RolesGuard } from "../../roles/roles.guard";
import { Roles } from "../../roles/roles.decorator";
import { Role } from "../../enum/role.enum";

@Controller('flight')
export class FlightController {
  constructor(private readonly flightService: FlightService) {}

  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @Get()
  findAll(
      @Query('page') page: number = Pagination.page,
      @Query('limit') limit: number = Pagination.limit,
      limitDto: LimitDto
  ) {
    return this.flightService.findAll({limit, page});
  }

  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.flightService.findOne(+id);
  }
}
