import {Injectable, NotFoundException} from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { CONNECTION_SHU } from '../../constants/Connection.constants';
import { IPaginationOptions, paginate, Pagination } from "nestjs-typeorm-paginate";
import Flight from "../../shu/dist/model/Flight";

@Injectable()
export class FlightService {
  constructor(@InjectRepository(Flight, CONNECTION_SHU) private flightRepository: Repository<Flight>) {}

  async findAll(options: IPaginationOptions): Promise<Pagination<Flight>> {
    const queryBuilder = this.flightRepository;

    return paginate<Flight>(queryBuilder, options);
  }

  async findOne(id: number): Promise<Flight> {
    let flight = await this.flightRepository.findOne(id);

    if (!flight)
      throw new NotFoundException();

    return flight;
  }
}
