import { Module } from '@nestjs/common';
import { TokenService } from './token.service';
import { TokenController } from './token.controller';
import { TypeOrmModule } from "@nestjs/typeorm";
import { TokenEntity } from "../../models/token.entity";
import { JwtStrategy } from "../../auth/jwt.strategy";
import {JwtModule} from "@nestjs/jwt";
import {ConfigModule, ConfigService} from "@nestjs/config";
import { UserEntity } from "../../models/user.entity";
import { CONNECTION_LOCAL } from '../../constants/Connection.constants';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      TokenEntity,
      UserEntity,
    ], CONNECTION_LOCAL),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => {
        return {
          secret: configService.get<string>('JWT_SECRET')
        };
      },
      inject: [ConfigService]
    }),
  ],
  controllers: [TokenController],
  providers: [
      TokenService,
      JwtStrategy
  ]
})
export class TokenModule {}
