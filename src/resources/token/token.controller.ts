import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  UseGuards,
  Request, Query
} from '@nestjs/common';
import { TokenService } from './token.service';
import { CreateTokenDto } from './dto/create-token.dto';
import { JwtAuthGuard } from "../../auth/jwt-auth.guard";
import { LimitDto } from "../../utils/dto/limit.dto";
import {Pagination} from "../../enum/pagination.enum";

@Controller('token')
export class TokenController {
  constructor(private readonly tokenService: TokenService) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  create(@Body() createTokenDto: CreateTokenDto) {
    return this.tokenService.create(createTokenDto);
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  findAll(
      @Request() req,
      @Query('page') page: number = Pagination.page,
      @Query('limit') limit: number = Pagination.limit,
      limitDto: LimitDto
  ) {
    return this.tokenService.findAll(req, {limit, page});
  }

  @UseGuards(JwtAuthGuard)
  @Get('/users')
  findAllUsers(@Request() req) {
    return this.tokenService.findAllUsers(req);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  remove(
      @Param('id') id: string,
      @Request() req: any
  ) {
    return this.tokenService.remove(+id, req);
  }
}
