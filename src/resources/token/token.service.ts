import {
  BadRequestException,
  Injectable,
  NotFoundException
} from '@nestjs/common';
import { CreateTokenDto } from './dto/create-token.dto';
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import {
  paginateRaw,
  Pagination,
  IPaginationOptions
} from 'nestjs-typeorm-paginate';
import { JwtService } from "@nestjs/jwt";
import { UserEntity } from "../../models/user.entity";
import { TokenEntity } from "../../models/token.entity";
import { CONNECTION_LOCAL } from '../../constants/Connection.constants';

@Injectable()
export class TokenService {
  constructor(
      @InjectRepository(TokenEntity, CONNECTION_LOCAL) private tokenRepository: Repository<TokenEntity>,
      @InjectRepository(UserEntity, CONNECTION_LOCAL) private userRepository: Repository<UserEntity>,
      private jwtService: JwtService
  ) {}

  async create(createTokenDto: CreateTokenDto) {
    let user = await this.userRepository.findOne(createTokenDto.user_id);

    if (user == undefined)
      throw new BadRequestException();

    let token = new TokenEntity();

    const payload = {
      id: user.id,
      isAdmin: user.is_admin,
      api_list: createTokenDto.ips
    };

    let access_token = this.jwtService.sign(payload);

    token.is_expired = false;
    token.user = user;
    token.token = access_token;

    await this.tokenRepository.save(token);

    return {
      message: 'created successfully',
      access_token: access_token
    };
  }

  async findAll(req: any, options: IPaginationOptions): Promise<Pagination<TokenEntity>> {
    let where = {};

    if (!req.user.isAdmin)
      where['user'] = req.user.id;

    const queryBuilder = this.tokenRepository
        .createQueryBuilder()
        .leftJoin('users', 'UserEntity', 'UserEntity.id = TokenEntity.user_id')
        .select('TokenEntity.id, TokenEntity.user_id, token, TokenEntity.created_at, UserEntity.name')
        .where(where);

    return paginateRaw(queryBuilder, options);
  }

  async findAllUsers(req: any) {
    let data = [];
    let where = {};

    if (req.user.isAdmin)
      where['id'] = req.user.id;

    let users = await this.userRepository.find(where);

    for (let user of users) {
      data.push({
        text: user.name,
        value: user.id
      });
    }

    return data;
  }

  async remove(id: number, req: any) {
    let token = await this.tokenRepository.findOne(id);

    if (!req.user.isAdmin && token.id !== req.user.id) {
      throw new NotFoundException();
    }

    let deleted = await this.tokenRepository.delete(id);

    if (!deleted.affected)
      throw new NotFoundException();

    return {
      message: 'deleted successfully'
    };
  }
}
