import {
    IsArray,
    IsNotEmpty, Validate
} from "class-validator";
import { IsCorrectApiList } from "../../../utils/validation/check-apis.validate";

export class CreateTokenDto {
    @IsArray()
    @IsNotEmpty()
    @Validate(IsCorrectApiList)
    ips: string[]

    @IsNotEmpty()
    user_id: number
}
