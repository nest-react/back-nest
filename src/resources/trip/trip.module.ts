import { Module } from '@nestjs/common';
import { TripService } from './trip.service';
import { TripController } from './trip.controller';
import { TypeOrmModule } from "@nestjs/typeorm";
import { JwtStrategy } from "../../auth/jwt.strategy";
import { CONNECTION_SHU } from '../../constants/Connection.constants';
import { Trip } from '../../shu/dist/model/Trip';

@Module({
  imports: [
      TypeOrmModule.forFeature([
          Trip
      ], CONNECTION_SHU)
  ],
  controllers: [TripController],
  providers: [
      TripService,
      JwtStrategy
  ]
})
export class TripModule {}
