import { Injectable } from '@nestjs/common';
import { IPaginationOptions, paginate, Pagination } from "nestjs-typeorm-paginate";
import { InjectRepository } from "@nestjs/typeorm";
import { CONNECTION_SHU } from "../../constants/Connection.constants";
import { Repository } from "typeorm";
import { Trip } from "../../shu/dist/model/Trip";


@Injectable()
export class TripService {
  constructor(@InjectRepository(Trip, CONNECTION_SHU) private tripRepository: Repository<Trip>) {}

  async findAll(options: IPaginationOptions): Promise<Pagination<Trip>> {
    const queryBuilder = this.tripRepository;

    return paginate<Trip>(queryBuilder, options);
  }
}
