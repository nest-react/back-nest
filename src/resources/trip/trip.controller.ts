import {Controller, Get, Param, Query, UseGuards} from '@nestjs/common';
import { TripService } from './trip.service';
import { JwtAuthGuard } from "../../auth/jwt-auth.guard";
import { RolesGuard } from "../../roles/roles.guard";
import { Roles } from "../../roles/roles.decorator";
import { Role } from "../../enum/role.enum";
import { Pagination } from "../../enum/pagination.enum";
import { LimitDto } from "../../utils/dto/limit.dto";

@Controller('trip')
export class TripController {
  constructor(private readonly tripService: TripService) {}

  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @Get()
  findAll(
      @Query('page') page: number = Pagination.page,
      @Query('limit') limit: number = Pagination.limit,
      limitDto: LimitDto
  ) {
    return this.tripService.findAll({limit, page});
  }
}
