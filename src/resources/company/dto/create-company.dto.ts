import {
    IsNotEmpty,
    MinLength,
    Validate,
} from "class-validator";
import { IsUnique } from '../../../utils/validation/unique.validate';
import Company  from "../../../shu/dist/model/Company";
import { CONNECTION_SHU } from "../../../constants/Connection.constants";

export class CreateCompanyDto {
    @IsNotEmpty()
    @MinLength(2)
    @Validate(IsUnique, [Company, 'name', CONNECTION_SHU])
    name: string

    credits: number

    firestoreData: string

    @IsNotEmpty()
    administrator: number

    firestoreId: string
}
