import { Module } from '@nestjs/common';
import { CompanyService } from './company.service';
import { CompanyController } from './company.controller';
import { JwtStrategy } from "../../auth/jwt.strategy";
import {TypeOrmModule} from "@nestjs/typeorm";
import { CONNECTION_SHU } from "../../constants/Connection.constants";
import Company from "../../shu/dist/model/Company";
import User from "../../shu/dist/model/User";

@Module({
  imports: [
    TypeOrmModule.forFeature([
        Company,
        User,
    ], CONNECTION_SHU)
  ],
  controllers: [CompanyController],
  providers: [
      CompanyService,
      JwtStrategy
  ]
})
export class CompanyModule {}
