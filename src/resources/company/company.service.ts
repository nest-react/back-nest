import {BadRequestException, Injectable, NotFoundException} from '@nestjs/common';
import { CreateCompanyDto } from './dto/create-company.dto';
import { UpdateCompanyDto } from './dto/update-company.dto';
import { InjectRepository } from "@nestjs/typeorm";
import { CONNECTION_SHU } from "../../constants/Connection.constants";
import { Repository } from "typeorm";
import { IPaginationOptions, paginate, Pagination } from "nestjs-typeorm-paginate";
import Company from "../../shu/dist/model/Company";
import User from "../../shu/dist/model/User";

@Injectable()
export class CompanyService {
  constructor(
      @InjectRepository(Company, CONNECTION_SHU) private companyRepository: Repository<Company>,
      @InjectRepository(User, CONNECTION_SHU) private userRepository: Repository<User>,
  ) {}

  async create(createCompanyDto: CreateCompanyDto) {
    let company = new Company();

    let user = await this.userRepository.findOne(createCompanyDto.administrator);

    if (!user)
      throw new BadRequestException();

    company.name = createCompanyDto.name;
    company.credits = createCompanyDto.credits;
    company.firestoreData = createCompanyDto.firestoreData;
    company.firestoreId = createCompanyDto.firestoreId;
    company.administrator = user;

    await this.companyRepository.save(company);

    return {
      message: 'created successfully'
    };
  }

  async findAll(options: IPaginationOptions): Promise<Pagination<Company>> {
    const queryBuilder = this.companyRepository;

    return paginate<Company>(queryBuilder, options);
  }

  async findOne(id: number): Promise<Company> {
    let company = await this.companyRepository.findOne(id);

    if (!company)
      throw new NotFoundException();

    return company;
  }

  async update(id: number, updateCompanyDto: UpdateCompanyDto) {
    let company = await this.companyRepository.findOne(id);

    if (!company)
      throw new NotFoundException();

    if (updateCompanyDto.administrator) {
      let user = await this.userRepository.findOne(updateCompanyDto.administrator);

      if (!user)
        throw new BadRequestException();

      company.administrator = user;
    }

    company.name = updateCompanyDto.name;
    company.credits = updateCompanyDto.credits;
    company.firestoreData = updateCompanyDto.firestoreData;
    company.firestoreId = updateCompanyDto.firestoreId;

    await this.companyRepository.save(company);

    return {
      message: 'updated successfully'
    };
  }

  async remove(id: number) {
    let deleted = await this.companyRepository.delete(id);

    if (!deleted.affected)
      throw new NotFoundException();

    return {
      message: 'deleted successfully'
    };
  }
}
