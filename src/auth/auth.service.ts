import {Injectable, UnauthorizedException} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserEntity } from '../models/user.entity';
import { TokenEntity } from '../models/token.entity';
import { CONNECTION_LOCAL } from '../constants/Connection.constants';
import { LoginFrontDto } from './dto/login-front.dto';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UserEntity, CONNECTION_LOCAL) private userRepository: Repository<UserEntity>,
    @InjectRepository(TokenEntity, CONNECTION_LOCAL) private tokenRepository: Repository<TokenEntity>,
    private jwtService: JwtService
  ) {}

  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.userRepository.findOne({
      where: {
        name: username
      }
    });

    if (user) {
      return {
        id: user.id,
        name: user.name,
        is_admin: user.is_admin
      };
    }

    return null;
  }

  async loginAuth0(req: any, loginDto: LoginFrontDto) {
    let user = await this.userRepository.findOne({
      where: {
        user_id: loginDto.user_id
      }
    });

    if (!user)
      throw new UnauthorizedException();

    req.user = {
      id: user.id,
      is_admin: user.is_admin,
    };

    let access_token = await this.login(req.user,['all']);

    return {
      access_token: access_token.access_token,
      is_admin: user.is_admin
    };
  }

  async login(user: any, api_list: string[]) {
    const payload = {
      id: user.id,
      is_admin: user.is_admin,
      api_list
    };

    let access_token = this.jwtService.sign(payload);
    let token_entity = new TokenEntity();

    token_entity.token = access_token;
    token_entity.user = user.id;

    await this.tokenRepository.save(token_entity);

    return { access_token };
  }

  async logout(token: string) {
    let wb_token = token.slice(7);

    let token_row = await this.tokenRepository.findOne({
      where: {
        token: wb_token
      }
    });

    if (token_row.is_expired)
      throw new UnauthorizedException();

    token_row.is_expired = true;

    await this.tokenRepository.save(token_row);

    return {
      message: "logout is done"
    };
  }
}