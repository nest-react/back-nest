"use strict";

import { IsNotEmpty } from "class-validator";

export class LoginFrontDto {
    @IsNotEmpty()
    user_id: string;
}
