"use strict";

import { IsCorrectApiList } from '../../utils/validation/check-apis.validate';

import {
    IsArray,
    IsNotEmpty,
    Validate
} from "class-validator";

export class LoginDto {
    @IsNotEmpty()
    @IsArray()
    @Validate(IsCorrectApiList)
    apis: string[];

    @IsNotEmpty()
    username: string;

    @IsNotEmpty()
    password: string;
}
