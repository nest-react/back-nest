import {
    Body,
    Controller,
    Post,
    Request,
    UseGuards,
    Headers,
    UnauthorizedException
} from "@nestjs/common";
import { LocalAuthGuard } from './local-auth.guard';
import { AuthService } from './auth.service';
import { LoginDto } from './dto/login.dto';
import { JwtAuthGuard } from './jwt-auth.guard';
import { ConfigService } from '@nestjs/config';
import { LoginFrontDto } from './dto/login-front.dto';

@Controller()
export class AuthController {
    constructor(
      private authService: AuthService,
      private configService: ConfigService
    ) {}

    @Post('auth0/login')
    loginAuth0(
      @Body() loginDto: LoginFrontDto,
      @Request() req
    ) {
        req.user = {};

        if (req.headers.origin !== this.configService.get<string>('CLIENT_URL'))
            throw new UnauthorizedException();

        return this.authService.loginAuth0(req, loginDto);
    }

    @UseGuards(LocalAuthGuard)
    @Post('login/generate-token')
    loginFront(
        @Body() loginDto: LoginDto,
        @Request() req
    ) {
        return this.authService.login(req.user, loginDto.apis);
    }

    @UseGuards(JwtAuthGuard)
    @Post('logout')
    logout(@Headers('authorization') token: string) {
        return this.authService.logout(token);
    }
}
