import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { PassportModule } from '@nestjs/passport';
import { LocalStrategy } from './local.strategy';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './jwt.strategy';
import { AuthController } from './auth.controller';
import { TypeOrmModule } from "@nestjs/typeorm";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { UserEntity } from "../models/user.entity";
import { TokenEntity } from "../models/token.entity";
import { CONNECTION_LOCAL } from '../constants/Connection.constants';

@Module({
  imports: [
    PassportModule,
    TypeOrmModule.forFeature([
      UserEntity,
      TokenEntity
    ], CONNECTION_LOCAL),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => {
        return {
          secret: configService.get<string>('JWT_SECRET')
        };
      },
      inject: [ConfigService]
    }),
  ],
  providers: [
      AuthService,
      LocalStrategy,
      JwtStrategy,
  ],
  exports: [AuthService],
  controllers: [AuthController],
})
export class AuthModule {}