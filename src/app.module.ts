import { HttpModule, Module } from "@nestjs/common";
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './auth/auth.module';
import { AirlineModule } from './resources/airline/airline.module';
import { LogEndpointsModule } from './resources/log-endpoints/log-endpoints.module';
import { ServeStaticModule } from "@nestjs/serve-static";
import { join } from 'path';
import { TokenModule } from './resources/token/token.module';
import { PassengerModule } from './resources/passenger/passenger.module';
import { CompanyModule } from './resources/company/company.module';
import { FlightModule } from './resources/flight/flight.module';
import { TripModule } from './resources/trip/trip.module';
import { UserTripModule } from './resources/user-trip/user-trip.module';
import { SnakeNamingStrategy } from "typeorm-naming-strategies";
import { CONNECTION_SHU, CONNECTION_LOCAL } from "./constants/Connection.constants";
import { ScheduleModule } from "@nestjs/schedule";
import { CronService } from "./services/cron.service";

@Module({
  imports: [
      ServeStaticModule.forRoot({
          rootPath: join(__dirname, '../..', '/client/dist')
      }),
      ConfigModule.forRoot({
          envFilePath: '.env',
          isGlobal: true
      }),
      ScheduleModule.forRoot(),
      TypeOrmModule.forRootAsync({
          imports: [
              ConfigModule
          ],
          inject: [ConfigService],
          name: CONNECTION_LOCAL,
          useFactory: (configService: ConfigService) => {
              return {
                  name: CONNECTION_LOCAL,
                  type: 'postgres',
                  host: configService.get<string>("DB_HOST", "localhost"),
                  port: configService.get<number>("DB_PORT", 5432),
                  username: configService.get<string>("DB_USERNAME", "postgres"),
                  password: configService.get<string>("DB_PASSWORD", "postgres"),
                  database: configService.get<string>("DB_DATABASE_NAME", "shu_api"),
                  synchronize: true,
                  logging: configService.get<string>("NODE_ENV", "development") === "development",
                  namingStrategy: new SnakeNamingStrategy(),
                  entities: [__dirname + '/**/models/**.entity.js'],
              }
          }
      }),
      TypeOrmModule.forRootAsync({
          name: CONNECTION_SHU,
          inject: [ConfigService],
          useFactory: (configService: ConfigService) => {
              return {
                  name: CONNECTION_SHU,
                  type: 'postgres',
                  host: configService.get<string>("SHU_DB_HOST", "localhost"),
                  port: configService.get<number>("SHU_DB_PORT", 5432),
                  username: configService.get<string>("SHU_DB_USERNAME", "postgres"),
                  password: configService.get<string>("SHU_DB_PASSWORD", "postgres"),
                  database: configService.get<string>("SHU_DB_DATABASE_NAME", "shu"),
                  synchronize: false,
                  logging: configService.get<string>("NODE_ENV", "development") === "development",
                  namingStrategy: new SnakeNamingStrategy(),
                  entities: [__dirname + '/**/dist/model/**.js'],
              }
          }
      }),
      HttpModule,
      AuthModule,
      AirlineModule,
      LogEndpointsModule,
      TokenModule,
      PassengerModule,
      CompanyModule,
      FlightModule,
      TripModule,
      UserTripModule,
  ],
    providers: [
      CronService
    ]
})
export class AppModule {}
